# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).
This file follows the formats and conventions from [keepachangelog.com]

## [Unreleased]
### Solved
### Added

[1.1.10]
 - Improved performance of the taurus tree for archived attributes.

[1.1.9]
 - Modified queries to use local copies of archived attrs.

[1.1.8]
 - Handled tree browser not showing attributes from stopped devices.

[1.1.5]
 - CHanged background color of the trend

[1.1.4]
 - First entry of the changelog
 - Added Tango Archiving Tree Browser, extracted from PyTangoArchiving

[1.1.3]
 - Added wrapper to launch this GUI from ctarchiving old tool in case both are installed.

[1.1.2]
 - Added export to text option from pyhdbpp.

[1.1.1]
 - Added docked trend.
 - Added buttons to create new trends and forms.
 - Added rearranging options to select time range window.

[1.1.0]
 - First public GUI version
 - Added drag and drop functionality with models.



[keepachangelog.com]: http://keepachangelog.com
[Unreleased]: https://gitlab.com/tango-controls/hdbpp/libhdbpp-tangobrowser/-/tree/main


