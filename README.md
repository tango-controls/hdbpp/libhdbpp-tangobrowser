### TangoBrowser

A tango and archiving tool to look for attributes in use or ever archived.
This tool is a graphical interface to search tango attributes archived or not.

## Installation
```bash
pip install tangobrowser
```

## Usage
```bash
tango_browser
```
